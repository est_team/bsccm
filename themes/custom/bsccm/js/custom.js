(function ($) {
    $('.region-navigation nav').meanmenu({
        meanMenuContainer: '#mobilemenu',
        meanScreenWidth: '772'
    });

    let btn = $('#button');

    $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
    });


    // homepage blog listing
    let blog = -1;
    let blog_content = $('.blog-thumbnail-content');
    blog_content.each(function() {
        blog = blog > $(this).height() ? blog : $(this).height();
    });

    blog_content.each(function() {
        $(this).height(blog);
    });
    let count = 0;

    $('.field--name-field-references .field--items > .field--item').each(function () {
        count++;
        let text = $(this).html();
        $(this).html("<span>"+count+".</span><div class='ref-text'>"+text+"</div>");
    });
    $('body.page-node-type-publication section.section').append('<a href="javascript:void(0)" class="print-journal"><i class="fas fa-print"></i>Print</a>');
    $(document).on('click', '.print-journal', function () {
        $('#main').printThis({
            loadCSS: '/themes/custom/bsccm/css/publication-pdf.css'
        });
    });
})(jQuery);