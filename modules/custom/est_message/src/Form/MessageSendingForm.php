<?php

namespace Drupal\est_message\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\est_membership\Utility\UtilityManager;
use  \Drupal\user\Entity\User;

/**
 * Class MessageSendingForm.
 */
class MessageSendingForm extends FormBase {

    const ALL_USER = 'all_user';
    const COMMITTEE_MEMBER = 'committee_member';
    const EVENT_REG_MEMBER = 'event_reg_user';
    const MOBILE_NUMBER_PATTERN = '/^(?:\+88|01)?(?:\d{11}|\d{13})$/';


    private $message;

    private $config;

    public function __construct()
    {
        $this->message = \Drupal::service('est_message.sending');
        $this->config = \Drupal::config(MessageConfigForm::MESSAGE_CONF);
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'message_sending_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $form['receivers'] = [
            '#type' => 'select',
            '#title' => $this->t('Receivers'),
            '#options' => [
                static::ALL_USER => $this->t('All Members'),
                static::COMMITTEE_MEMBER => $this->t('Committee Member'),
                static::EVENT_REG_MEMBER => $this->t('Event Register')
            ],
            '#size' => 1,
            '#weight' => '0',
        ];
        $form['event_name'] = [
            '#type' => 'select',
            '#title' => $this->t('Event Name'),
            '#options' => UtilityManager::getEvents(),
            '#states' => array(
                // Only show this field when the value of type is sell.
                'visible' => array(
                    ':input[name="receivers"]' => array('value' => static::EVENT_REG_MEMBER),
                ),
            ),
            '#size' => 1,
            '#weight' => '0',
        ];
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Send Message'),
            '#attributes' => ['class' => ['button--primary']],
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $mobileNumberList = [];
        $event = null;
        $receiver = $form_state->getValue('receivers');
        if ($receiver == static::EVENT_REG_MEMBER) {
            $event = $form_state->getValue('event_name');
        }

        switch ($receiver) {
            case static::ALL_USER:
                 $mobileNumberList = $this->getUserMobileNumber('user', 'all');
                break;
            case static::COMMITTEE_MEMBER:
                $mobileNumberList = $this->getUserMobileNumber('user', 'field_position');
                break;
            case static::EVENT_REG_MEMBER:
                $mobileNumberList = $this->getEventParticipantMobileNumber($event);
                break;
        }
        if ($message = $this->config->get(MessageConfigForm::BULK_SMS)) {
            $this->message->setType('OneToMany');
            $this->message->send($mobileNumberList, $message);
        }

    }


    /**
     * @param $table
     * @param $condition
     * @return array
     */
    private function getUserMobileNumber($table, $condition) {
        $mobileNumbers = [];
        $query = \Drupal::entityQuery($table);
        $ids = $query->execute();
        $users = User::loadMultiple($ids);

        foreach ($users as $user) {
            $number = $user->field_telephone_mobile->value;
            $position = $user->field_position->value;

            if (!is_null($number) && preg_match(static::MOBILE_NUMBER_PATTERN, $number) && $condition == 'all') {
                $mobileNumbers[] = $number;
            }
            elseif (!is_null($number) && preg_match(static::MOBILE_NUMBER_PATTERN, $number) && $position != null) {
                $mobileNumbers[] = $number;
            }
        }
        return implode(',', $mobileNumbers);
    }

    /**
     * @param $event
     * @return array
     */
    private function getEventParticipantMobileNumber($event) {
        $mobileNumbers = [];
        $database = \Drupal::service('database');
        $select = $database->select('webform_submission_data', 'wsd')
            ->fields('wsd', array('value'))
            ->condition('wsd.webform_id', $event, '=')
            ->condition('wsd.name', 'phone', '=');
        $executed = $select->execute();
        $results = $executed->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($results as $result) {
            if (!is_null($result['value']) && preg_match(static::MOBILE_NUMBER_PATTERN, $result['value'])) {
                $mobileNumbers[] = $result['value'];
            }
        }
        return implode(',', $mobileNumbers);
    }

}
