<?php

namespace Drupal\est_message\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MessageConfigForm.
 */
class MessageConfigForm extends ConfigFormBase {
    /**
     * All constant
     */
    const MESSAGE_CONF = 'est_message.settings';
    const USERNAME = 'username';
    const PASSWORD = 'password';
    const EVENT_REG_MESSAGE = 'event_reg_message';
    const EVENT_REG_MESSAGE_FLAG = 'event_reg_message_flag';
    const USER_REG_MESSAGE = 'user_reg_message';
    const USER_REG_MESSAGE_FLAG = 'user_reg_message_flag';
    const BULK_SMS = 'bulk_message';

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            static::MESSAGE_CONF
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'message_config_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config(static::MESSAGE_CONF);
        $form[static::USERNAME] = [
            '#type' => 'textfield',
            '#title' => $this->t('Username'),
            '#description' => $this->t('OnnorokomSMS Username'),
            '#maxlength' => 18,
            '#size' => 64,
            '#default_value' => $config->get(static::USERNAME),
            '#required' => True,
        ];
        $form[static::PASSWORD] = [
            '#type' => 'textfield',
            '#title' => $this->t('Password'),
            '#description' => $this->t('OnnorokomSMS Password'),
            '#maxlength' => 6,
            '#size' => 64,
        ];
        $form['event_reg'] = array(
            '#type' => 'fieldset',
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
            '#title' => $this
                ->t('Event Registration Message Settings'),
        );
        $form['event_reg'][static::EVENT_REG_MESSAGE] = [
            '#type' => 'textarea',
            '#title' => $this->t('Event Registration Message'),
            '#default_value' => $config->get(static::EVENT_REG_MESSAGE),
            '#description' => $this->t('Replacement Pattern @@name@@ @@amount@@ and @@currency@@')
        ];
        $form['event_reg'][static::EVENT_REG_MESSAGE_FLAG] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Event Registration Message Enable'),
            '#default_value' => $config->get(static::EVENT_REG_MESSAGE_FLAG),
        ];

        $form['user_reg'] = array(
            '#type' => 'fieldset',
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
            '#title' => $this
                ->t('User Registration Message Settings'),
        );
        $form['user_reg'][static::USER_REG_MESSAGE] = [
            '#type' => 'textarea',
            '#title' => $this->t('User Registration / Payment Message'),
            '#default_value' => $config->get(static::USER_REG_MESSAGE),
            '#description' => $this->t('Replacement Pattern @@name@@ @@amount@@ and @@currency@@')
        ];
        $form['user_reg'][static::USER_REG_MESSAGE_FLAG] = [
            '#type' => 'checkbox',
            '#title' => $this->t('User Registration / Payment Message Enable'),
            '#default_value' => $config->get(static::USER_REG_MESSAGE_FLAG),
        ];


        $form['bulk'] = array(
            '#type' => 'fieldset',
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
            '#title' => $this
                ->t('Bulk Message Settings'),
        );
        $form['bulk'][static::BULK_SMS] = [
            '#type' => 'textarea',
            '#title' => $this->t('User Bulk Message'),
            '#default_value' => $config->get(static::BULK_SMS),
        ];
        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        parent::submitForm($form, $form_state);

        $this->config(static::MESSAGE_CONF)
            ->set(static::USERNAME, $form_state->getValue(static::USERNAME))
            ->set(static::PASSWORD, $form_state->getValue(static::PASSWORD))
            ->set(static::EVENT_REG_MESSAGE, $form_state->getValue(static::EVENT_REG_MESSAGE))
            ->set(static::EVENT_REG_MESSAGE_FLAG, $form_state->getValue(static::EVENT_REG_MESSAGE_FLAG))
            ->set(static::USER_REG_MESSAGE, $form_state->getValue(static::USER_REG_MESSAGE))
            ->set(static::USER_REG_MESSAGE_FLAG, $form_state->getValue(static::USER_REG_MESSAGE_FLAG))
            ->set(static::BULK_SMS, $form_state->getValue(static::BULK_SMS))
            ->save();
    }

}
