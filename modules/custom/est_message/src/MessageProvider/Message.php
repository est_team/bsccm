<?php
/**
 * Created by PhpStorm.
 * User: hizbul
 * Date: 4/20/19
 * Time: 1:10 PM
 */
namespace Drupal\est_message\MessageProvider;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\est_message\Form\MessageConfigForm;

class Message
{

    private $userName;

    private $password;

    private $config;

    private $messageType;

    public function __construct()
    {
        $this->config = \Drupal::config(MessageConfigForm::MESSAGE_CONF);
        $this->userName = $this->config->get(MessageConfigForm::USERNAME);
        $this->password = $this->config->get(MessageConfigForm::PASSWORD);
    }

    public function setType($messageType) {
        $this->messageType = $messageType;
    }

    public function send($to, $message) {
        $soapClient = new \SoapClient("https://api2.onnorokomSMS.com/sendSMS.asmx?wsdl");
        $onnorokomArray = [
            'userName'      => $this->userName,
            'userPassword'  => $this->password,
            'mobileNumber'  => $to,
            'smsText'       => $message,
            'type'          => 'TEXT',
            'maskName'      => 'NewOffer',
            'campaignName'  => ''
        ];
        if ($this->messageType == 'OneToMany') {
            $onnorokomArray['numberList'] = $to;
            $onnorokomArray['messageText'] = isset($message) ? $message : 'Hello from BSCCM.';
            unset($onnorokomArray['mobileNumber']);
            unset($onnorokomArray['smsText']);
        }

        try{
            $value = $soapClient->__call($this->messageType, [$onnorokomArray]);
            $func = $this->messageType . 'Result';
            $arrResult = explode("||", $value->$func);

            \Drupal::messenger()->addMessage('Message send successfully!!', MessengerInterface::TYPE_STATUS);

            return $arrResult;
        }
        catch (\Exception $ex)
        {
            return [0 => '9999', 1 => $ex->getMessage()];
        }

    }
}