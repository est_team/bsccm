(function ($) {
    'use strict';

    Drupal.behaviors.PresentationSlide = {
        attach: function(context, settings) {
            let height = -1;
            let slide = $('.item');
            slide.each(function() {
                height = height > $(this).height() ? height : $(this).height();
            });

            slide.each(function() {
                $(this).height(height);
            });
        }
    };

}(jQuery));