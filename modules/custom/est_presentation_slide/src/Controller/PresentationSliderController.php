<?php

namespace Drupal\est_presentation_slide\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class PresentationSliderController.
 */
class PresentationSliderController extends ControllerBase {

    public function presentation($id) {
        $node =  \Drupal\node\Entity\Node::load($id);
        $content = $node->body->getValue();
        $this->setTitle($node);

        return $render = [
            '#theme' => 'slider_show',
            '#data' => $content,
            '#attached' => [
                'library' => [
                    'est_presentation_slide/slider',
                ]
            ]
        ];
    }

    private function setTitle($node) {
        if ($title = $node->title->value) {
            $request = \Drupal::request();
            if ($route = $request->attributes->get(\Symfony\Cmf\Component\Routing\RouteObjectInterface::ROUTE_OBJECT)) {
                $route->setDefault('_title', $title);
            }
        }
    }

}
