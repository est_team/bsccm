<?php
/**
 * Created by PhpStorm.
 * User: hizbul
 * Date: 4/7/19
 * Time: 11:52 AM
 */

namespace Drupal\est_member_importer\Form;


use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

class MemberImportForm extends FormBase
{

    /**
     * Returns a unique string identifying the form.
     *
     * The returned ID should be a unique string that can be a valid PHP function
     * name, since it's used in hook implementation names such as
     * hook_form_FORM_ID_alter().
     *
     * @return string
     *   The unique string identifying the form.
     */
    public function getFormId()
    {
        return 'est_member_importer_form';
    }

    /**
     * Form constructor.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return array
     *   The form structure.
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['excel_file'] = [
            '#type' => 'managed_file',
            '#title' => $this->t('Excel File'),
            '#upload_location' => 'public://member_import',
            '#multiple' => TRUE,
            '#name' => 'excel_file',
            '#required' => TRUE,
            '#upload_validators' => array(
                'file_validate_extension' => array('xls', 'xlsx')
            ),
            '#description' => t('xls format only. <a href="/sites/default/files/member_import/member_list.xls">Excel File structure</a>'),
        ];


        // Add a submit button that handles the submission of the form.
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Submit'),
            '#attributes' => ['class' => ['button--primary']],
        ];

        return $form;
    }
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        if ($form_state->getValue('excel_file') == NULL) {
            $form_state->setErrorByName('excel_file', $this->t('File.'));
        }

        parent::validateForm($form, $form_state);
    }

    /**
     * Form submission handler.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $form_file = $form_state->getValue('excel_file', 0);
        if (isset($form_file[0]) && !empty($form_file[0])) {
            $file = File::load($form_file[0]);
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($file->getFileUri());
            $worksheet = $spreadsheet->getActiveSheet();
            $users = [];

            foreach ($worksheet->getRowIterator() as $row) {
                $cellIterator = $row->getCellIterator();
                foreach ($cellIterator as $cell) {
                    if ($cell->getRow() == 1) continue;
                    $users[$cell->getRow()][] = $cell->getValue();
                }
            }
            $count = $this->createUser($users);
            $file->setPermanent();
            $file->save();
            \Drupal::messenger()->addMessage(t(count($count) . ' users imported successfully!'));
        }
    }

    private function createUser(array  $users) {
        $ids = [];
        $language = \Drupal::languageManager()->getCurrentLanguage()->getId();


        foreach ($users as $user) {
            if (!isset($user[1])) continue;
            $objUser = \Drupal\user\Entity\User::create();
            $objUser->setPassword('bsccm123456');
            $objUser->enforceIsNew();
            $objUser->setEmail($user[1]);
            $objUser->setUsername(explode('@', $user[1])[0]);
            $objUser->set('init', $user[1]);
            $objUser->set('langcode', $language);
            $objUser->set('preferred_langcode', $language);
            $objUser->set('preferred_admin_langcode', $language);
            $objUser->addRole('blogger');
            $objUser->set('field_telephone_mobile', $user[2]);
            $objUser->set('field_name', $user[0]);
            $objUser->set('field_membership_category', 'Life');
            $objUser->set('field_present_appointment', $user[3]);
            $objUser->set('field_work_place_affliation', $user[4]);
            $objUser->set('field_payment_method', 'Bank');
            $objUser->activate();
            try {
               if ( $objUser->save() ) {
                   $ids[] = $objUser->id();
               }
            }
            catch (EntityStorageException $e) {
                \Drupal::messenger()->addMessage(t('Could not create user (username: %username) (email: %email) exception: %e', [
                    '%e' => $e->getMessage(),
                    '%username' => explode('@', $user[1])[0],
                    '%email' => $user[1]
                ]), 'error');
            }
        }
        return $ids;
    }

}


