<?php

namespace Drupal\est_member_importer\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class MemberImportController.
 */
class MemberImportController extends ControllerBase {

  /**
   * Import.
   *
   * @return string
   *   Return Hello string.
   */
  public function import() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: import')
    ];
  }

}
