<?php

namespace Drupal\est_member_importer\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the est_member_importer module.
 */
class MemberImportControllerTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "est_member_importer MemberImportController's controller functionality",
      'description' => 'Test Unit for module est_member_importer and controller MemberImportController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests est_member_importer functionality.
   */
  public function testMemberImportController() {
    // Check that the basic functions of module est_member_importer.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
