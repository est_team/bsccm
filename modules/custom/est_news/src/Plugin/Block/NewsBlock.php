<?php
namespace Drupal\est_news\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "est_news_block",
 *   admin_label = @Translation("News Block"),
 * )
 */
class NewsBlock extends BlockBase {

    /**
     * Builds and returns the renderable array for this block plugin.
     *
     * If a block should not be rendered because it has no content, then this
     * method must also ensure to return no content: it must then only return an
     * empty array, or an empty array with #cache set (with cacheability metadata
     * indicating the circumstances for it being empty).
     *
     * @return array
     *   A renderable array representing the content of the block.
     *
     * @see \Drupal\block\BlockViewBuilder
     */
    public function build()
    {
        $query = \Drupal::entityQuery('node');
        $query->condition('status', 1);
        $query->condition('type', 'news');
        $query->sort('created' , 'DESC');
        $query->range(0, 5);
        $nids = $query->execute();

        $node_storage = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);

        return [
            '#markup' => create_news_block($node_storage)
        ];
    }

}