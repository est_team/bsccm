(function ($) {
    'use strict';

    Drupal.behaviors.MembershipPage = {
        attach: function(context, settings) {
            let member = -1;
            let card = $('.member-content');
            card.each(function() {
                member = member > $(this).height() ? member : $(this).height();
            });

            card.each(function() {
                $(this).height(member);
            });
        }
    };

}(jQuery));