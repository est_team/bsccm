<?php
/**
 * Created by PhpStorm.
 * User: hizbul
 * Date: 4/6/19
 * Time: 3:51 PM
 */

namespace Drupal\est_membership\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

class MembershipController extends ControllerBase
{
    public function showPaymentInfo() {
        $header = [
            ['data' => $this->t('Sr')],
            ['data' => $this->t('User'), 'field' => 'uid', 'sort' => 'asc'],
            ['data' => $this->t('Amount')],
            ['data' => $this->t('Last Payment Date'), 'field' => 'created', 'sort' => 'DESC'],
            ['data' => $this->t('Next Payment Date')],
            ['data' => $this->t('Status')],
            ['data' => $this->t('Action')],
        ];
        $connection = \Drupal::service('database');
        $query = $connection->select('est_membership_payment', 'emp');
        $query->join('users', 'u', 'u.uid = emp.uid');
        $query
            ->fields('emp', array('id', 'uid', 'amount', 'created', 'next_payment', 'status'))
            ->orderBy('emp.created', 'ASC');
        $tableSort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
            ->orderByHeader($header);
        $pager = $tableSort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
            ->limit(15);
        $result = $pager->execute()->fetchAll(\PDO::FETCH_ASSOC);
        $rows = array();
        foreach ($result as $content) {
            $account = \Drupal\user\Entity\User::load($content['uid']);
            $nextPaymentDate = !is_null($content['next_payment']) ? date("d-m-Y g:i:s A", $content['next_payment']) : null;
            $url = Url::fromRoute('entity.user.canonical', ['user' => $content['uid']]);
            $status = $content['status'] ? 'Done' : 'Pending';
            $value = $content['status'] ? 0 : 1;
            $op = $content['status'] ? 'Reset' : 'Complete';
            $rows[] = [
                'data' => [
                    $content['id'],
                    \Drupal::l(t($account->getUsername()), $url),
                    $content['amount'],
                    date("d-m-Y g:i:s A", $content['created']),
                    $nextPaymentDate,
                    $status,
                    \Drupal::l(t($op), Url::fromRoute('est_membership.member_payment_status_update', ['id' => $content['id'], 'value' => $value])),
                ]
            ];
        }

        $output['table_data'] = array(
            '#theme' => 'table',
            '#header' => $header,
            '#rows' => $rows
        );
        $output['pager'] = ['#type' => 'pager'];

        return $output;
    }

    /**
     * @param Request $request
     * @return string|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updatePayment(Request $request) {
        try {
            $database = \Drupal::service('database');
            $result = $database->update('est_membership_payment')
                ->fields([
                    'status' => $request->get('value'),
                ])
                ->condition('id', $request->get('id'), '=')
                ->execute();
        }
        catch (\Exception $exception) {
            return $exception->getMessage();
        }

        if ($result) {
            \Drupal::messenger()->addMessage(t('Payment status updated.'), 'success');
        }
        else {
            \Drupal::messenger()->addMessage(t('There is some error.'), 'error');
        }

        return $this->redirect('est_membership.member_payment');
    }

}