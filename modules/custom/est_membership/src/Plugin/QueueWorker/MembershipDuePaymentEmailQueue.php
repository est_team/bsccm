<?php
/**
 * Created by PhpStorm.
 * User: hizbul
 * Date: 4/24/19
 * Time: 11:01 AM
 */

namespace Drupal\est_membership\Plugin\QueueWorker;


use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Processes tasks for Membership Email Queue
 *
 * @QueueWorker(
 *   id = "est_membership_queue",
 *   title = @Translation("Example: Membership Email Queue"),
 *   cron = {"time" = 120}
 * )
 */
class MembershipDuePaymentEmailQueue extends QueueWorkerBase
{
    /**
     * {@inheritdoc}
     */
    public function processItem($data)
    {
        $mailManager = \Drupal::service('plugin.manager.mail');
        $module = 'est_membership';
        $to = $data;
        $params['message'] = "Hello Sir, \n\n  Hope you’re well. This is just to remind you that payment on bsccm membership, which is due. I’m sure you’re busy, but I’d appreciate if you could take a moment and look over the payment when you get a chance. Please let me know if you have any questions. \n\n - BSCCM Team.";
        $params['title'] = 'Due Payment Reminder';
        $langcode = \Drupal::currentUser()->getPreferredLangcode();
        $send = true;

        $result = $mailManager->mail($module, 'membership_due_mail', $to, $langcode, $params, NULL, $send);
    }
}