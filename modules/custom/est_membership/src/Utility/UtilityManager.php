<?php
/**
 * Created by PhpStorm.
 * User: hizbul
 * Date: 4/6/19
 * Time: 11:41 AM
 */

namespace Drupal\est_membership\Utility;

Use \Drupal\user\Entity\User;

class UtilityManager
{
    /**
    * Membership Type
    */
    const GENERAL = 'General';
    const ASSOCIATE = 'Associate';
    const LIFE = 'Life';
    const BASIC = 'Basic';
    const BLOGGER = 'blogger'; //role

    public static function getTotalPayAmount($user) {
        $amount = 0;
        switch ($user->field_membership_category->value) {
            case 'Life':
                $amount = 10000;
                break;
            case 'General':
                $amount = 1000;
                break;
            case 'Associate':
                $amount = 400;
        }
        return $amount;
    }

    public static function getUserRenewAmount($user) {
        $amount = 0;
        switch ($user->field_membership_category->value) {
            case 'Life':
                $amount = 0;
                break;
            case 'General':
                $amount = 500;
                break;
            case 'Associate':
                $amount = 200;
        }
        return $amount;
    }

    public static function getNextPaymentDate($user) {
        $date = null;
        if ($user->field_membership_category->value != 'Life') {
            $date = strtotime('+1 year', \Drupal::time()->getRequestTime());
        }
        return $date;
    }

    public static function getUsers() {
        $arrUser = [];
        $users = User::loadMultiple();
        foreach ($users as $id => $user) {
            if ($id > 1) {
                $arrUser[$id] = $user->field_name->value;
            }
        }
        return $arrUser;
    }

    /**
     * @param $uid
     * @param $form_state
     * @param $state
     * @return mixed
     */
    public static function createPaymentByUser($uid, $form_state, $state= True) {
        $amount = $form_state->getValue('amount');
        $created = strtotime($form_state->getValue('created'));
        $nextPaymentDate = strtotime('+' . $form_state->getValue('year') .' year', $created);
        $database = \Drupal::service('database');
        return $database->insert('est_membership_payment')
            ->fields([
                'uid' => $uid,
                'amount' => $amount,
                'created' => $created,
                'next_payment' => $nextPaymentDate,
                'status' => $state
            ])
            ->execute();
    }

    /**
     * Get events
     */
    public static function getEvents() {
        $events = [];
        $database = \Drupal::service('database');
        $select = $database->select('webform')
            ->fields('webform', array('webform_id'))
            ->condition('webform_id', 'criticon_%', 'LIKE');
        $executed = $select->execute();
        $results = $executed->fetchAll(\PDO::FETCH_ASSOC);
        if (sizeof($results) < 1)
            return [];
        foreach ($results[0] as $key => $value) {
            $events[$value] = ucfirst(str_replace('_', ' ', $value));
        }
        return $events;
    }
}