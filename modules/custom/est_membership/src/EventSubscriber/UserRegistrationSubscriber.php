<?php
/**
 * Created by PhpStorm.
 * User: hizbul
 * Date: 4/1/19
 * Time: 4:37 PM
 */

namespace Drupal\est_membership\EventSubscriber;

use Drupal\est_membership\Event\UserRegistrationEvent;
use Drupal\est_membership\Payment\OnlinePayment;
use Drupal\est_membership\Utility\UtilityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\user\Entity\User;

class UserRegistrationSubscriber implements EventSubscriberInterface
{

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents() {
        return [
            UserRegistrationEvent::EVENT_NAME => 'onUserRegistration'
        ];
    }


    /**
     * @param UserRegistrationEvent $event
     * @return mixed
     */
    public function onUserRegistration(UserRegistrationEvent $event) {
        $user = User::load($event->account->id());
        $user->addRole(UtilityManager::BLOGGER);
        $user->activate();
        $user->save();
        $sslCommerz = \Drupal::service('est_membership.sslcommerz');
        $payment = new OnlinePayment($user);

        return $sslCommerz->initiate($payment->getPaymentData());
    }
}