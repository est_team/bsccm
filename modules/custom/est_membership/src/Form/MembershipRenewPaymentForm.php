<?php
/**
 * Created by PhpStorm.
 * User: hizbul
 * Date: 4/7/19
 * Time: 11:52 AM
 */

namespace Drupal\est_membership\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\est_membership\Payment\OnlinePayment;
use Drupal\est_membership\Utility\UtilityManager;
Use \Drupal\user\Entity\User;

class MembershipRenewPaymentForm extends FormBase
{
    const ONLINE_PAYMENT = 'Online Payment';
    const BANK = 'Bank';
    const CASH = 'Cash';

    /**
     * Returns a unique string identifying the form.
     *
     * The returned ID should be a unique string that can be a valid PHP function
     * name, since it's used in hook implementation names such as
     * hook_form_FORM_ID_alter().
     *
     * @return string
     *   The unique string identifying the form.
     */
    public function getFormId()
    {
        return 'membership_payment_renew_form';
    }

    /**
     * Form constructor.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return array
     *   The form structure.
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['#attributes']['class'] = ['form-horizontal'];
        $form['amount'] = [
            '#type' => 'number',
            '#title' => $this->t('Amount'),
            '#description' => $this->t('Payment amount in BDT'),
            '#field_suffix' => 'BDT',
            '#required' => TRUE,
        ];

        $form['year'] = [
            '#type' => 'number',
            '#title' => $this->t('Renew Year'),
            '#description' => $this->t('Payment for years, eg. 1, 2, 3..'),
            '#default_value' => 1,
            '#required' => TRUE,
        ];

        $form['created'] = [
            '#type' => 'date',
            '#title' => 'Payment Date',
            '#description' => 'Payment Date',
            '#required' => TRUE,
        ];

        $form['payment_method'] = [
            '#type' => 'radios',
            '#title' => $this->t(self::ONLINE_PAYMENT),
            '#required' => TRUE,
            '#default_value' => self::ONLINE_PAYMENT,
            '#options' => array(
                self::ONLINE_PAYMENT => $this
                    ->t(self::ONLINE_PAYMENT),
                self::BANK => $this
                    ->t(self::BANK),
                self::CASH => $this
                    ->t(self::CASH),
            ),
        ];

        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Submit'),
            '#attributes' => ['class' => ['button--primary']],
        ];

        return $form;
    }
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        $user = User::load($this->currentUser()->id());
        $amount = $form_state->getValue('amount');
        $fee = UtilityManager::getTotalPayAmount($user);
        $renewalCost = UtilityManager::getUserRenewAmount($user) * $form_state->getValue('year');

        if ($form_state->getValue('year') < 1) {
            $form_state->setErrorByName('year', $this->t('Year can\'t be negative value.'));
        }
        if (($amount / UtilityManager::getUserRenewAmount($user)) != $form_state->getValue('year')) {
            $form_state->setErrorByName('amount', $this->t('You have to pay ' . $renewalCost . ' BDT. to extend ' . $form_state->getValue('year') . ' year membership.'));
        }

        parent::validateForm($form, $form_state);
    }

    /**
     * Form submission handler.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $uid = $this->currentUser()->id();
        if ($form_state->getValue('payment_method') != self::ONLINE_PAYMENT){
            UtilityManager::createPaymentByUser($uid, $form_state, False);
        }
        $sslCommerz = \Drupal::service('est_membership.sslcommerz');
        $payment = new OnlinePayment(User::load($uid));
        $payment->setTotalAmount($form_state->getValue('amount'));
        $payment->setPaymentMethod($form_state->getValue('payment_method'));

        return $sslCommerz->initiate($payment->getPaymentData());
    }

}