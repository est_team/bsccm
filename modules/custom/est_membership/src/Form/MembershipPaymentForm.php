<?php
/**
 * Created by PhpStorm.
 * User: hizbul
 * Date: 4/7/19
 * Time: 11:52 AM
 */

namespace Drupal\est_membership\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\est_membership\Utility\UtilityManager;
Use \Drupal\user\Entity\User;

class MembershipPaymentForm extends FormBase
{

    /**
     * Returns a unique string identifying the form.
     *
     * The returned ID should be a unique string that can be a valid PHP function
     * name, since it's used in hook implementation names such as
     * hook_form_FORM_ID_alter().
     *
     * @return string
     *   The unique string identifying the form.
     */
    public function getFormId()
    {
        return 'membership_payment_form';
    }

    /**
     * Form constructor.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return array
     *   The form structure.
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['payment_type'] = [
            '#type' => 'radios',
            '#title' => $this->t('Payment Type'),
            '#required' => TRUE,
            '#default_value' => 2,
            '#options' => array(
                1 => $this
                    ->t('New'),
                2 => $this
                    ->t('Renew'),
            ),
        ];

        $form['uid'] = [
            '#type' => 'select',
            '#title' => $this->t('User'),
            '#options' => UtilityManager::getUsers(),
            '#description' => $this->t('Select User.'),
            '#required' => TRUE,
        ];

        $form['amount'] = [
            '#type' => 'number',
            '#title' => $this->t('Amount'),
            '#description' => $this->t('Payment amount in BDT'),
            '#field_suffix' => 'BDT',
            '#required' => TRUE,
        ];

        $form['year'] = [
            '#type' => 'number',
            '#title' => $this->t('Renew Year'),
            '#description' => $this->t('Payment for years, eg. 1, 2, 3..'),
            '#states' => array(
                // Only show this field when the value of type is sell.
                'invisible' => array(
                    ':input[name="payment_type"]' => array('value' => 1),
                ),
            ),
            '#default_value' => 1,
            '#required' => TRUE,
        ];

        $form['created'] = [
            '#type' => 'date',
            '#title' => 'Payment Date',
            '#description' => 'Payment Date',
            '#required' => TRUE,
        ];

        // Add a submit button that handles the submission of the form.
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Submit'),
            '#attributes' => ['class' => ['button--primary']],
        ];

        return $form;
    }
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        $type = $form_state->getValue('payment_type');
        $user = User::load($form_state->getValue('uid'));
        $amount = $form_state->getValue('amount');
        $fee = UtilityManager::getTotalPayAmount($user);
        $renewalCost = UtilityManager::getUserRenewAmount($user) * $form_state->getValue('year');

        if ($form_state->getValue('year') < 1) {
            $form_state->setErrorByName('year', $this->t('Year can\'t be negative value.'));
        }
        if ($type == 2 && ($amount / UtilityManager::getUserRenewAmount($user)) != $form_state->getValue('year')) {
            $form_state->setErrorByName('amount', $this->t('You have to pay ' . $renewalCost . ' BDT.'));
        }
        elseif ($type == 1 && $amount != $fee) {
            $form_state->setErrorByName('amount', $this->t('You have to pay ' . $fee . ' BDT.'));
        }
        parent::validateForm($form, $form_state);
    }

    /**
     * Form submission handler.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $uid = $form_state->getValue('uid');
        $result = UtilityManager::createPaymentByUser($uid, $form_state);
        if ($result) {
            \Drupal::messenger()->addMessage(t('Payment done.'), MessengerInterface::TYPE_STATUS);
        }
        else {
            \Drupal::messenger()->addMessage(t('There is some error.'), MessengerInterface::TYPE_ERROR);
        }
    }

}