<?php
/**
 * Created by PhpStorm.
 * User: hizbul
 * Date: 4/1/19
 * Time: 4:21 PM
 */

namespace Drupal\est_membership\Event;


use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\Event;

class UserRegistrationEvent extends Event
{
    const EVENT_NAME = 'est_membership_user_registration';

    /**
     * The user account.
     *
     * @var \Drupal\user\UserInterface
     */
    public $account;


    /**
     * Constructs the object.
     *
     * @param \Drupal\user\UserInterface $account
     *   The account of the user logged in.
     */
    public function __construct(UserInterface $account) {
        $this->account = $account;
    }

}