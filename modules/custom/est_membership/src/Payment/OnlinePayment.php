<?php
/**
 * Created by PhpStorm.
 * User: hizbul
 * Date: 4/16/19
 * Time: 1:08 PM
 */

namespace Drupal\est_membership\Payment;


use Drupal\est_membership\Utility\UtilityManager;
use Drupal\est_payment\Form\EventConfigForm;
use Drupal\user\Entity\User;

class OnlinePayment
{
    /**
     * User Payment Information
     * @var array
     */
    private $data = [];

    /**
     * User Object
     * @var User
     */
    private $user;

    /**
     * User Payment amount
     * @var int
     */
    private $totalAmount = 0;

    /**
     * Payment Method
     * @var string
     */
    private $paymentMethod = 'Online Payment';

    /**
     * OnlinePayment constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->totalAmount = UtilityManager::getTotalPayAmount ($user);
        $this->paymentMethod = $user->field_payment_method->value;
    }

    /**
     * @param $amount
     */
    public function setTotalAmount($amount) {
        $this->totalAmount = $amount;
    }

    /**
     * @param $method
     */
    public function setPaymentMethod($method) {
        $this->paymentMethod = $method;
    }

    /**
     * Get Payment Data
     * @return array
     */
    public function getPaymentData() {
        global $base_url;
        $config = \Drupal::config('est_payment.settings');

        if ($this->paymentMethod != 'Online Payment' || $this->user->field_membership_category->value == UtilityManager::BASIC) {
            $this->data['id'] = $this->user->id();
            $this->data['name'] = $this->user->field_name->value;
            $this->data['email'] = $this->user->mail->value;
            $this->data['country'] = 'Bangladesh';
            $this->data['header'] = 'Membership Registration';
            $this->data['message'] =  'Your registration is done. Please pay first and contact with BSCCM Team.';
            $this->data['total_amount'] = $this->totalAmount;

            return $this->renderTemplate('payment_process', $this->data);
        }
        else {
            $this->data['store_id'] = $config->get('est_payment_sslcommerz_store_id');
            $this->data['store_passwd'] = $config->get('est_payment_sslcommerz_store_pass');
            $this->data['total_amount'] = $this->totalAmount;
            $this->data['currency'] = 'BDT';
            $this->data['tran_id'] = $this->user->id();
            $this->data['cus_name'] = $this->user->field_name->value;
            $this->data['cus_email'] = $this->user->mail->value;
            $this->data['cus_phone'] = $this->user->field_telephone_mobile->value;
            $this->data['payment_type'] = 'userRegistration';
            $this->data['emi_option'] = 0;
            $this->data['url'] = '/register';
            $this->data['fail_url'] = $base_url . $config->get(EventConfigForm::EVENT_FAIL_URL);
            $this->data['success_url'] = $base_url . '/membership-payment';
            $this->data['cancel_url'] = $base_url . '/user/register';

            return $this->data;
        }
    }

    /**
     * @param $theme
     * @param $data
     * @return array
     */
    private function renderTemplate($theme, $data) {
        return $render = [
            '#theme' => $theme,
            '#data' => $data,
            '#attached' => [
                'library' => [
                    'est_payment/est-styling',
                ]
            ]
        ];
    }

}