CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Modal Page module allow create Modal in CMS and set page to show. If the 
user visit this page that configures it shows the modal.


REQUIREMENTS
------------

No special requirements


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-8 for further
information.


CONFIGURATION
-------------

 * Configure your messages in Administration » Structure » Modal

  Click in Add Modal
 
   - Set the Title of modal.
   - Set the Text of modal (Body).
   - Set type of modal. 
   - Set de pages or parameters to show the modal.
   - Set text for OK label button.
   - Choose modal language.
   - Save.


MAINTAINERS
-----------

Current maintainers:
 * Renato Gonçalves (RenatoG) - https://www.drupal.org/user/3326031
