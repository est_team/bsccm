<?php
/**
 * Created by PhpStorm.
 * User: hizbul
 * Date: 4/15/19
 * Time: 6:11 PM
 */

namespace Drupal\modal_page\Event;

use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\Event;

class UserLoginEvent extends Event
{
    const EVENT_NAME = 'est_user_login';

    /**
     * The user account.
     *
     * @var \Drupal\user\UserInterface
     */
    public $account;


    /**
     * Constructs the object.
     *
     * @param \Drupal\user\UserInterface $account
     *   The account of the user logged in.
     */
    public function __construct(UserInterface $account) {
        $this->account = $account;
    }
}