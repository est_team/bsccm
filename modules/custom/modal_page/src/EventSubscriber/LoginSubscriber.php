<?php

namespace Drupal\modal_page\EventSubscriber;

use Drupal\modal_page\Event\UserLoginEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\est_membership\Utility\UtilityManager;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LoginSubscriber.
 */
class LoginSubscriber implements EventSubscriberInterface {
    /**
     * {@inheritdoc}
     */
    static function getSubscribedEvents() {
        return [
            UserLoginEvent::EVENT_NAME => 'onUserLogin'
        ];
    }

    /**
     * This method is called whenever the auth event is
     * dispatched.
     *
     * @param GetResponseEvent $event
     */
    public function onUserLogin(Event $event) {
        $user = User::load($event->account->id());
        $memberType = $user->field_membership_category->value;
        $roles = $user->getRoles();
        if (in_array('login', explode('/', $_SERVER['HTTP_REFERER']))) {
            if (in_array(UtilityManager::BLOGGER, $roles)) {
                $response = new RedirectResponse("/?modal=membership");
                return $response->send();
            }
            elseif (in_array($memberType, [UtilityManager::GENERAL, UtilityManager::ASSOCIATE]) && $this->isPaymentDue($user)) {
                $response = new RedirectResponse("/?modal=renew");
                return $response->send();
            }
        }
    }


    /**
     * @param User $user
     * @return bool
     */
    private function isPaymentDue(User $user) {
        $database = \Drupal::service('database');
        $select = $database->select('est_membership_payment', 'emp')
            ->fields('emp', array('next_payment'))
            ->condition('emp.uid', $user->id(), '=')
            ->orderBy('emp.next_payment', 'DESC')
            ->range(0, 1);
        $executed = $select->execute();
        return $executed->fetchColumn() <= time() ;
    }

}
