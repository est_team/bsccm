<?php

namespace Drupal\est_payment\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\est_message\Form\MessageConfigForm;
use Drupal\est_payment\Form\EventConfigForm;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\est_membership\Utility\UtilityManager;


class PaymentController extends ControllerBase {

    const SUCCESS = 'Success';
    const PENDING = 'Pending';
    const FAILED = 'Failed';
    
    public function __construct() {
        $this->sslCommerz = \Drupal::service('est_membership.sslcommerz');
        $this->message = \Drupal::service('est_message.sending');
        $this->config = \Drupal::config(MessageConfigForm::MESSAGE_CONF);
    }

    public function payment() {
        $data = null;
        $eventConf = \Drupal::config(EventConfigForm::SETTINGS);
        if (!array_key_exists('tran_id', $_POST)) {
            return new RedirectResponse(EventConfigForm::EVENT_URL);
        }
        $tranId = $_POST['tran_id'];
        $result = $this->webFormSubmissionBySerial($tranId, $eventConf->get(EventConfigForm::EVENT_NAME));
        \Drupal::logger('est_payment')->log('info', "Sid is: " . $result['sid']);

        $submission = $this->entityTypeManager()->getStorage('webform_submission')->load($result['sid']);
        if (!empty($submission)) {
            $data = $submission->getData();
            if (!array_key_exists('sid', $data)) {
                $data['sid'] = $tranId;
                $data['name'] = $data['name']['first'] .' '. $data['name']['last'];
            }
        }
        else {
            \Drupal::messenger()->addMessage(t('No Data found with this id.'), 'error');
        }

        if($data['payment_status'] == self::PENDING && array_key_exists('payment_info', $_SESSION)) {
            $amount = $_SESSION['payment_info']['total_amount'];
            $currency = $_SESSION['payment_info']['currency'];
            $_SESSION['payment_info']['name'] = $_SESSION['payment_info']['cus_name'];
            $validation = $this->sslCommerz->orderValidate($tranId, $amount, $currency, $_POST);
            if ($validation) {
                $result = $submission->setElementData('payment_status', self::SUCCESS)->save();
                if ($result == 2) {
                    $_SESSION['success'] = True;
                    $pass = bin2hex(random_bytes(5));
                    if ($this->createUserAccount($data, $pass)) {
                        $data['pass'] = $pass;
                        $this->sendEmail('auto_user_create', $this->getEmailContent($data));
                        if (MessageConfigForm::EVENT_REG_MESSAGE_FLAG)
                            $this->message->setType('OneToOne');
                            $this->message->send($_SESSION['payment_info']['cus_phone'][0], $this->generateTextMessage($this->config->get(MessageConfigForm::EVENT_REG_MESSAGE), $_SESSION['payment_info']));
                    }
                    $data['header'] = $eventConf->get(EventConfigForm::EVENT_TITLE);
                    $data['message'] = 'Your payment has been accepted successfully. Please check your email for details.';
                    return $this->renderTemplate('payment_process', $data);
                }
            }
        }
        elseif ($data['payment_status'] == self::SUCCESS) {
            return $this->renderTemplate('payment_failed', 'You have already completed your registration.');
        }
        return $this->renderTemplate('payment_failed', 'Something wrong happened!!');
    }

    /**
     * @return array|RedirectResponse
     */
    public function memberRegistrationPayment() {
        if (!array_key_exists('tran_id', $_POST)) {
            return $this->redirect('<front>');
        }
        $tranId = $_POST['tran_id'];
        $user = \Drupal\user\Entity\User::load($tranId);
        if ($user) {
            $memberType = $user->field_membership_category->value;
            $amount = UtilityManager::getTotalPayAmount ($user);
            if (in_array($memberType, [UtilityManager::ASSOCIATE, UtilityManager::GENERAL])) {
                $amount = UtilityManager::getUserRenewAmount($user);
            }
            $data['id'] = $tranId;
            $data['name'] = $user->field_name->value;
            $data['email'] = $user->mail->value;
            $data['country'] = 'Bangladesh';
            $data['total_amount'] = $amount;
            $data['currency'] = 'BDT';

            $validation = $this->sslCommerz->orderValidate($tranId, $amount, 'BDT', $_POST);
            if ($validation) {
                $result = $this->setUserPaymentAmount($tranId, $user, $amount);
                if ($result) {
                    $data['header'] = 'Membership Registration / Renew';
                    $data['message'] =  'Your payment has been accepted successfully. Please check your email for details.';
                    $this->sendEmail('user_online_payment', $this->getEmailContent($data));
                    if (MessageConfigForm::USER_REG_MESSAGE_FLAG)
                        $this->message->setType('OneToOne');
                        $this->message->send($user->field_telephone_mobile->value, $this->generateTextMessage($this->config->get(MessageConfigForm::USER_REG_MESSAGE), $data));
                    return $this->renderTemplate('payment_process', $data);
                }
            }
            else {
                return $this->renderTemplate('payment_failed', 'Ooops!! Invalid Payment');
            }
        }
        else {
            return $this->renderTemplate('payment_failed', 'No Data found with this user id!!');
        }
    }


    public function paymentFail() {
        $_SESSION['success'] = FALSE;
        return $this->renderTemplate('payment_failed', 'Something wrong happened!!');
    }

    /**
     * @param $theme
     * @param $data
     * @return array
     */
    private function renderTemplate($theme, $data) {
        return $render = [
            '#theme' => $theme,
            '#data' => $data,
            '#attached' => [
                'library' => [
                    'est_payment/est-styling',
                ]
            ]
        ];
    }

    private function webFormSubmissionBySerial($serial, $webFormId = 'criticon_2020')  {
        $database = \Drupal::service('database');
        $select = $database->select('webform_submission', 'sd')
            ->fields('sd', array('sid'))
            ->condition('sd.webform_id', $webFormId, '=')
            ->condition('sd.serial', $serial, '=');
        $executed = $select->execute();
        $results = $executed->fetchAll(\PDO::FETCH_ASSOC);
        if (count($results) == 1) {
            $results = reset($results);
        }
        return $results;
    }

    private function setUserPaymentAmount($tranId, $user, $amount) {
        $database = \Drupal::service('database');
        $result = $database->insert('est_membership_payment')
            ->fields([
                'uid' => $tranId,
                'amount' => $amount,
                'created' => \Drupal::time()->getRequestTime(),
                'next_payment' => UtilityManager::getNextPaymentDate($user),
                'status' => TRUE
            ])
            ->execute();
        return $result;
    }

    private function createUserAccount($data, $pass) {
        $userName = explode('@', $data['email'])[0];
        if (user_load_by_name($userName)) {
            \Drupal::messenger()->addMessage($this->t('User already exists with this username.'), 'error');
            return False;
        }
        $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $user = \Drupal\user\Entity\User::create();
        $user->setPassword($pass);
        $user->enforceIsNew();
        $user->setEmail($data['email']);
        $user->setUsername($userName);

        $user->set('init', $data['email']);
        $user->set('langcode', $language);
        $user->set('preferred_langcode', $language);
        $user->set('preferred_admin_langcode', $language);
        if ($user->save()) {
            $newUser = user_load_by_name($userName);
            $newUser->addRole(UtilityManager::BLOGGER);
            $newUser->activate();
            return $newUser->save();
        }
    }


    private function sendEmail($key, $mailAttrs) {
        $mailManager = \Drupal::service('plugin.manager.mail');
        $module = 'est_payment';
        $to = $mailAttrs['to'];
        $params['message'] = $mailAttrs['message'];
        $params['title'] = $mailAttrs['title'];
        $langcode = \Drupal::currentUser()->getPreferredLangcode();
        $send = true;

        $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
        if ($result['result'] != true) {
            $message = t('There was a problem sending your email notification to @email.', array('@email' => $to));
            \Drupal::messenger()->addMessage($message, 'warning');
            \Drupal::logger('mail-log')->error($message);
            return;
        }

        $message = t('An email notification has been sent to @email ', array('@email' => $to));
        \Drupal::messenger()->addMessage($message, MessengerInterface::TYPE_STATUS);
        \Drupal::logger('mail-log')->notice($message);
    }

    private function getEmailContent($data) {
        global $base_url;
        if (array_key_exists('pass', $data)){
            return $mailData = [
                'to' => $data['email'],
                'title' => 'New account created',
                'message' => "Hello " .$data['name']. ", \n An account has been created while you register the event. Please login through the link: " . $base_url. "user/login 
                            \n Username: " . explode('@', $data['email'])[0] . "\n Password: " . $data['pass'] . "\n\n - BSCCM Team"
            ];
        }
        return $mailData = [
            'to' => $data['email'],
            'title' => 'Payment Completed',
            'message' => "Hello " .$data['name']. ", \n Your payment has been accepted successfully. We have received ". $data['total_amount'] . " BDT. \n\n - BSCCM Team"
        ];

    }

    private function generateTextMessage($message, $data) {
        return str_replace(['@@name@@', '@@amount@@', '@@currency@@'], [$data['name'], $data['total_amount'], $data['currency']], $message);
    }
}