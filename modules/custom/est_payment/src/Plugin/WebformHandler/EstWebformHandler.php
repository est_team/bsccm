<?php

namespace Drupal\est_payment\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\webformSubmissionInterface;
use Drupal\est_payment\PaymentGateway\SslCommerzGateway\SslCommerz;
use Drupal\est_payment\Form\EventConfigForm;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "est_payment_form_handler",
 *   label = @Translation("Est Handler form handler"),
 *   category = @Translation("Form Handler"),
 *   description = @Translation("Do something extra with form submissions"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class EstWebformHandler extends WebformHandlerBase {

    /**
     * {@inheritdoc}
     */

    public function defaultConfiguration() {
        return [
            'submission_url' => 'https://api.example.org/SOME/ENDPOINT',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
        $form['submission_url'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Submission URL to api.example.org'),
            '#description' => $this->t('The URL to post the submission data to.'),
            '#default_value' => $this->configuration['submission_url'],
            '#required' => TRUE,
        ];
        return $form;
    }


    /**
     * {@inheritdoc}
     */
    public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE)
    {
        $currentPage = \Drupal::request()->query->get('page');
        if (!is_null($currentPage) && $currentPage == 'payment') {
            $totalAmount = $this->getTotalAmount($webform_submission->getData());
            $webform_submission->setElementData('total_amount', $totalAmount);
        }
    }

    public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
        $currentPage = \Drupal::request()->query->get('page');
        if (!is_null($currentPage) && $currentPage == 'payment') {
            $values = $webform_submission->getData();
            $amountWithCurrency = ($values['country'] == 'Bangladesh') ? 'BDT ' . $this->getTotalAmount($values) : $this->getTotalAmount($values) . ' USD';
            $form['elements']['payment']['amount_to_pay']['#template'] = $this->getTotalAmountTemplate($amountWithCurrency);
        }
    }


    public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {
        global $base_url;

        $config = \Drupal::config('est_payment.settings');
        $values = $webform_submission->getData();
        $sid = $webform_submission->serial();
        $sslCommerz = new SslCommerz();
        $data['store_id'] = $config->get('est_payment_sslcommerz_store_id');
        $data['store_passwd'] = $config->get('est_payment_sslcommerz_store_pass');
        $data['total_amount'] = trim($values['total_amount']);
        $data['currency'] = ($values['country'] == 'Bangladesh') ? 'BDT' : 'USD';
        $data['tran_id'] = $sid;
        $data['cus_name'] = $values['name']['first'] . ' ' . $values['name']['last'];
        $data['cus_email'] = $values['email'];
        $data['cus_phone'] = $values['phone'];
        $data['emi_option'] = 0;
        $data['fail_url'] = $base_url . $this->getConf()->get(EventConfigForm::EVENT_FAIL_URL);
        $data['success_url'] = $base_url . $this->getConf()->get(EventConfigForm::EVENT_SUCCESS_URL);
        $data['cancel_url'] = $base_url . $this->getConf()->get(EventConfigForm::EVENT_CANCEL_URL);

        unset($_SESSION['payment_info']);
        $_SESSION['payment_info'] = $data;
        $_SESSION['payment_info']['submission_id'] = $sid;
        $_SESSION['payment_info']['participant_id'] = $sid;

        $sslCommerz->initiate($data);
    }

    /**
     * @param $values
     * @return float|int
     */
    protected function getTotalAmount($values) {
        $amountParticipant = 0;
        $amountAccompany = 0;
        $amountWorkshop = 0;
        if (array_key_exists('country', $values) && !empty($values['country'])) {
            if ($values['country'] == 'Bangladesh') {
                if ($values['participant_type'] == 'Post Graduate Doctor') {
                    $amountParticipant += $this->getConf()->get(EventConfigForm::BD_PARTICIPANT_POST_GRADE);
                }
                else {
                    $amountParticipant += $this->getConf()->get(EventConfigForm::BD_PARTICIPANT_OTHERS);
                }
            }
            elseif (in_array($values['country'], ['India', 'Pakistan', 'Srilanka', 'Nepal', 'Bhutan', 'Afghanistan', 'Maldives'])) {
                $amountParticipant += $this->getConf()->get(EventConfigForm::SAARC_PARTICIPANT);
            }
            else {
                $amountParticipant += $this->getConf()->get(EventConfigForm::ABROAD_PARTICIPANT);
            }
        }

        if ($values['country'] == 'Bangladesh') {
            $amountAccompany += !empty($values['have_accompany']) ? count($values['accompany_name']) * $this->getConf()->get(EventConfigForm::BD_ACCOMPANY) : 0;
            foreach ($values['workshop_category'] as $category) {
                if (in_array('Critical Care Nursing', $values['workshop_category']) && $category == 'Critical Care Nursing') {
                    $amountWorkshop += $this->getConf()->get(EventConfigForm::BD_WORKSHOP_CCN);
                }
                elseif (in_array('Bronchoscopy', $values['workshop_category']) && $category == 'Bronchoscopy') {
                    $amountWorkshop += $this->getConf()->get(EventConfigForm::BD_WORKSHOP_BRONCHOSCOPY);
                }
                elseif (in_array($category, ['Mechanical Ventilation', 'ECG and Imaging', 'ABG'])){
                    $amountWorkshop += $this->getConf()->get(EventConfigForm::BD_WORKSHOP_OTHERS);
                }
            }
        }
        else {
            $amountAccompany += !empty($values['have_accompany']) ? count($values['accompany_name']) * $this->getConf()->get(EventConfigForm::ABROAD_ACCOMPANY) : 0;
            foreach ($values['workshop_category'] as $category) {
                if (in_array('Critical Care Nursing', $values['workshop_category']) && $category == 'Critical Care Nursing') {
                    $amountWorkshop += $this->getConf()->get(EventConfigForm::ABROAD_WORKSHOP_CCN);
                }
                elseif (in_array('Bronchoscopy', $values['workshop_category']) && $category == 'Bronchoscopy') {
                    $amountWorkshop += $this->getConf()->get(EventConfigForm::ABROAD_WORKSHOP_BRONCHOSCOPY);
                }
                elseif (in_array($category, ['Mechanical Ventilation', 'ECG and Imaging', 'ABG'])){
                    $amountWorkshop += $this->getConf()->get(EventConfigForm::ABROAD_WORKSHOP_OTHERS);
                }
            }
        }

        return $amountParticipant + $amountAccompany + $amountWorkshop;
    }


    private function getTotalAmountTemplate($amount) {
        $html = "<div class='panel panel-info'><div class='panel-heading'>Amount to Pay</div><div class='panel-body'>";
        $html .= "<div class='payment'><strong>Total Amount: </strong>". $amount ."</div>";
        $html .= "<div class='payment-message'>Please click on <strong>Confirm Registration</strong> button to continue with payment process or revise your information click on <strong>Previous</strong> button.</div></div></div>";
        return $html;
    }

    private function getConf() {
        return \Drupal::config(EventConfigForm::SETTINGS);
    }
}
