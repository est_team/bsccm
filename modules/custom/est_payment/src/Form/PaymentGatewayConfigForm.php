<?php

namespace Drupal\est_payment\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * @file
 * Contains \Drupal\est_payment\Form\ResponsiveslideshowForm.
 */
class PaymentGatewayConfigForm extends ConfigFormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'est_payment_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            'est_payment.settings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config('est_payment.settings');
        $form['est_payment_sslcommerz_store_id'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Store ID'),
            '#required' => TRUE,
            '#default_value' => $config->get('est_payment_sslcommerz_store_id'),
            '#weight' => 0,
            '#description' => $this->t('Enter the sslcommerz store ID.'),
        );
        $form['est_payment_sslcommerz_store_pass'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Store Password'),
            '#required' => TRUE,
            '#weight' => 1,
            '#description' => $this->t('Enter the sslcommerz store password.'),
        );
        $form['est_payment_sslcommerz_sandbox'] = array(
            '#type' => 'radios',
            '#title' => $this->t('Is Sandbox?'),
            '#description' => $this->t('Are you running on sandbox mode?.'),
            '#required' => TRUE,
            '#default_value' => $config->get('est_payment_sslcommerz_sandbox', 0),
            '#weight' => 2,
            '#options' => array(
                1 => $this
                    ->t('Yes'),
                0 => $this
                    ->t('No'),
            ),

        );
        $form['est_payment_sslcommerz_local'] = array(
            '#type' => 'radios',
            '#title' => $this->t('Is Local?'),
            '#description' => $this->t('Are you running on local environment?.'),
            '#required' => TRUE,
            '#default_value' => $config->get('est_payment_sslcommerz_local', 0),
            '#options' => array(
                1 => $this
                    ->t('Yes'),
                0 => $this
                    ->t('No'),
            ),
            '#weight' => 3,

        );
        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        if (!isset($form['est_payment_sslcommerz_store_id']['#value'])) {
            $form_state->setErrorByName('est_payment_sslcommerz_store_id', 'Field is required..');
        }
        if (!isset($form['est_payment_sslcommerz_store_pass']['#value'])) {
            $form_state->setErrorByName('est_payment_sslcommerz_store_pass', 'Field is required..');
        }
        if (!isset($form['est_payment_sslcommerz_sandbox']['#value'])) {
            $form_state->setErrorByName('est_payment_sslcommerz_sandbox', 'Field is required..');
        }
        if (!isset($form['est_payment_sslcommerz_local']['#value'])) {
            $form_state->setErrorByName('est_payment_sslcommerz_local', 'Field is required..');
        }
    }

    /**
     * Submit handler for est_payment_form form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $config = $this->config('est_payment.settings');
        $est_payment_sslcommerz_store_id = $form_state->getValue(['est_payment_sslcommerz_store_id']);
        $est_payment_sslcommerz_store_pass = $form_state->getValue(['est_payment_sslcommerz_store_pass']);
        $est_payment_sslcommerz_sandbox = $form_state->getValue(['est_payment_sslcommerz_sandbox']);
        $est_payment_sslcommerz_local = $form_state->getValue(['est_payment_sslcommerz_local']);

        $config->set('est_payment_sslcommerz_store_id', $est_payment_sslcommerz_store_id);
        $config->set('est_payment_sslcommerz_store_pass', $est_payment_sslcommerz_store_pass);
        $config->set('est_payment_sslcommerz_sandbox', $est_payment_sslcommerz_sandbox);
        $config->set('est_payment_sslcommerz_local', $est_payment_sslcommerz_local);
        $config->save();
        parent::submitForm($form, $form_state);
        drupal_flush_all_caches();
    }

}
