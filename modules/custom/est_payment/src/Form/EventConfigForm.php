<?php

namespace Drupal\est_payment\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\est_membership\Utility\UtilityManager;

/**
 * @file
 * Contains \Drupal\est_event\Form\ResponsiveslideshowForm.
 */
class EventConfigForm extends ConfigFormBase {

    const EVENT_NAME = 'event_name';
    const EVENT_TITLE = 'event_title';
    const SETTINGS = 'est_event.settings';
    const EVENT_URL = 'est_event_url';
    const EVENT_SUCCESS_URL = 'est_event_success_url';
    const EVENT_FAIL_URL = 'est_event_fail_url';
    const EVENT_CANCEL_URL = 'est_event_cancel_url';
    const BD_PARTICIPANT_POST_GRADE = 'bd_part_post_grad';
    const BD_PARTICIPANT_OTHERS = 'bd_part_others';
    const SAARC_PARTICIPANT = 'saarc_part';
    const ABROAD_PARTICIPANT = 'abroad_part';
    const BD_ACCOMPANY = 'bd_accompany';
    const ABROAD_ACCOMPANY = 'abrod_accompany';
    const BD_WORKSHOP_CCN = 'bd_workshop_ccn';
    const ABROAD_WORKSHOP_CCN = 'abroad_workshop_ccn';
    const BD_WORKSHOP_BRONCHOSCOPY = 'bd_workshop_bron';
    const ABROAD_WORKSHOP_BRONCHOSCOPY = 'abroad_workshop_bron';
    const BD_WORKSHOP_OTHERS = 'bd_workshop_others';
    const ABROAD_WORKSHOP_OTHERS = 'abroad_workshop_others';


    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'est_event_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            static::SETTINGS,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config( static::SETTINGS);
        $form['event'] = array(
            '#type' => 'fieldset',
            '#collapsible' => TRUE,
            '#collapsed' => FALSE,
            '#title' => $this
                ->t('Event Information Settings'),
        );
        $form['event_cost'] = array(
            '#type' => 'fieldset',
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
            '#title' => $this
                ->t('Event COST Settings'),
        );
        $form['event'][static::EVENT_TITLE] = [
            '#type' => 'textfield',
            '#title' => $this->t('Event Title'),
            '#required' => True,
            '#default_value' => $config->get(static::EVENT_TITLE),
        ];
        $form['event'][static::EVENT_NAME] = [
            '#type' => 'select',
            '#title' => $this->t('Event Name'),
            '#options' => UtilityManager::getEvents(),
            '#default_value' => $config->get(static::EVENT_NAME),
            '#size' => 1,
        ];
        $form['event'][static::EVENT_URL] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Event URL'),
            '#required' => TRUE,
            '#default_value' => $config->get(static::EVENT_URL),
            '#weight' => 0,
        );
        $form['event'][static::EVENT_SUCCESS_URL] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Success URL'),
            '#default_value' => $config->get(static::EVENT_SUCCESS_URL),
            '#required' => TRUE,
            '#weight' => 1,
        );
        $form['event'][static::EVENT_FAIL_URL] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Fail URL'),
            '#default_value' => $config->get(static::EVENT_FAIL_URL),
            '#required' => TRUE,
            '#weight' => 3,
        );

        $form['event'][static::EVENT_CANCEL_URL] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Cancel URL'),
            '#default_value' => $config->get(static::EVENT_CANCEL_URL),
            '#required' => TRUE,
            '#weight' => 4,
        );

        $form['event_cost'][static::BD_PARTICIPANT_POST_GRADE] = array(
            '#type' => 'number',
            '#title' => $this->t('BD Participant - Post Grad Doctor'),
            '#required' => TRUE,
            '#default_value' => $config->get(static::BD_PARTICIPANT_POST_GRADE, 2000),
            '#description' => $this->t('Currency BDT'),
            '#weight' => 0,
        );
        $form['event_cost'][static::BD_PARTICIPANT_OTHERS] = array(
            '#type' => 'number',
            '#title' => $this->t('BD Participant - Others'),
            '#default_value' => $config->get(static::BD_PARTICIPANT_OTHERS, 1000),
            '#description' => $this->t('Currency BDT'),
            '#required' => TRUE,
            '#weight' => 1,
        );
        $form['event_cost'][static::SAARC_PARTICIPANT] = array(
            '#type' => 'number',
            '#title' => $this->t('SAARC - Participant'),
            '#default_value' => $config->get(static::SAARC_PARTICIPANT, 100),
            '#description' => $this->t('Currency USD'),
            '#required' => TRUE,
            '#weight' => 3,
        );

        $form['event_cost'][static::ABROAD_PARTICIPANT] = array(
            '#type' => 'number',
            '#title' => $this->t('Abroad - Participant'),
            '#default_value' => $config->get(static::ABROAD_PARTICIPANT, 200),
            '#description' => $this->t('Currency USD'),
            '#required' => TRUE,
            '#weight' => 4,
        );

        $form['event_cost'][static::BD_ACCOMPANY] = array(
            '#type' => 'number',
            '#title' => $this->t('BD - Accompany'),
            '#default_value' => $config->get(static::BD_ACCOMPANY, 1000),
            '#description' => $this->t('Currency BDT'),
            '#required' => TRUE,
            '#weight' => 5,
        );

        $form['event_cost'][static::ABROAD_ACCOMPANY] = array(
            '#type' => 'number',
            '#title' => $this->t('Abroad - Accompany'),
            '#default_value' => $config->get(static::ABROAD_ACCOMPANY, 100),
            '#description' => $this->t('Currency USD'),
            '#required' => TRUE,
            '#weight' => 6,
        );

        $form['event_cost'][static::BD_WORKSHOP_CCN] = array(
            '#type' => 'number',
            '#title' => $this->t('BD Worskshop - CCN'),
            '#default_value' => $config->get(static::BD_WORKSHOP_CCN, 1000),
            '#description' => $this->t('Currency BDT'),
            '#required' => TRUE,
            '#weight' => 7,
        );

        $form['event_cost'][static::ABROAD_WORKSHOP_CCN] = array(
            '#type' => 'number',
            '#title' => $this->t('Abroad Worskshop - CCN'),
            '#default_value' => $config->get(static::ABROAD_WORKSHOP_CCN, 50),
            '#description' => $this->t('Currency USD'),
            '#required' => TRUE,
            '#weight' => 8,
        );

        $form['event_cost'][static::BD_WORKSHOP_BRONCHOSCOPY] = array(
            '#type' => 'number',
            '#title' => $this->t('BD Worskshop - Bronchoscopy'),
            '#default_value' => $config->get(static::BD_WORKSHOP_BRONCHOSCOPY, 3000),
            '#description' => $this->t('Currency BDT'),
            '#required' => TRUE,
            '#weight' => 9,
        );

        $form['event_cost'][static::ABROAD_WORKSHOP_BRONCHOSCOPY] = array(
            '#type' => 'number',
            '#title' => $this->t('Abroad Worskshop - Bronchoscopy'),
            '#default_value' => $config->get(static::BD_WORKSHOP_BRONCHOSCOPY, 150),
            '#description' => $this->t('Currency USD'),
            '#required' => TRUE,
            '#weight' => 10,
        );

        $form['event_cost'][static::BD_WORKSHOP_OTHERS] = array(
            '#type' => 'number',
            '#title' => $this->t('BD Worskshop - Others'),
            '#default_value' => $config->get(static::BD_WORKSHOP_OTHERS, 2000),
            '#description' => $this->t('Currency BDT'),
            '#required' => TRUE,
            '#weight' => 11,
        );

        $form['event_cost'][static::ABROAD_WORKSHOP_OTHERS] = array(
            '#type' => 'number',
            '#title' => $this->t('Abroad Worskshop - Others'),
            '#default_value' => $config->get(static::ABROAD_WORKSHOP_OTHERS, 100),
            '#description' => $this->t('Currency USD'),
            '#required' => TRUE,
            '#weight' => 12,
        );

        return parent::buildForm($form, $form_state);
    }



    /**
     * Submit handler for est_event_form form.
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $this->configFactory->getEditable(static::SETTINGS)
            ->set(static::EVENT_TITLE, $form_state->getValue(static::EVENT_TITLE))
            ->set(static::EVENT_NAME, $form_state->getValue(static::EVENT_NAME))
            ->set(static::EVENT_URL, $form_state->getValue(static::EVENT_URL))
            ->set(static::EVENT_SUCCESS_URL, $form_state->getValue(static::EVENT_SUCCESS_URL))
            ->set(static::EVENT_FAIL_URL, $form_state->getValue(static::EVENT_FAIL_URL))
            ->set(static::EVENT_CANCEL_URL, $form_state->getValue(static::EVENT_CANCEL_URL))
            ->set(static::BD_PARTICIPANT_POST_GRADE, $form_state->getValue(static::BD_PARTICIPANT_POST_GRADE))
            ->set(static::BD_PARTICIPANT_OTHERS, $form_state->getValue(static::BD_PARTICIPANT_OTHERS))
            ->set(static::SAARC_PARTICIPANT, $form_state->getValue(static::SAARC_PARTICIPANT))
            ->set(static::ABROAD_PARTICIPANT, $form_state->getValue(static::ABROAD_PARTICIPANT))
            ->set(static::BD_ACCOMPANY, $form_state->getValue(static::BD_ACCOMPANY))
            ->set(static::ABROAD_ACCOMPANY, $form_state->getValue(static::ABROAD_ACCOMPANY))
            ->set(static::BD_WORKSHOP_CCN, $form_state->getValue(static::BD_WORKSHOP_CCN))
            ->set(static::ABROAD_WORKSHOP_CCN, $form_state->getValue(static::ABROAD_WORKSHOP_CCN))
            ->set(static::BD_WORKSHOP_BRONCHOSCOPY, $form_state->getValue(static::BD_WORKSHOP_BRONCHOSCOPY))
            ->set(static::ABROAD_WORKSHOP_BRONCHOSCOPY, $form_state->getValue(static::ABROAD_WORKSHOP_BRONCHOSCOPY))
            ->set(static::BD_WORKSHOP_OTHERS, $form_state->getValue(static::BD_WORKSHOP_OTHERS))
            ->set(static::ABROAD_WORKSHOP_OTHERS, $form_state->getValue(static::ABROAD_WORKSHOP_OTHERS))
            ->save();
        parent::submitForm($form, $form_state);
        drupal_flush_all_caches();
    }
}


