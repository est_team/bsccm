<?php
namespace Drupal\est_blog\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "est_blog_taxonomy_block",
 *   admin_label = @Translation("View By Category"),
 * )
 */
class TaxonomyBlock extends BlockBase {

    /**
     * Builds and returns the renderable array for this block plugin.
     *
     * If a block should not be rendered because it has no content, then this
     * method must also ensure to return no content: it must then only return an
     * empty array, or an empty array with #cache set (with cacheability metadata
     * indicating the circumstances for it being empty).
     *
     * @return array
     *   A renderable array representing the content of the block.
     *
     * @see \Drupal\block\BlockViewBuilder
     */
    public function build()
    {
        $query = \Drupal::entityQuery('taxonomy_term');
        $query->condition('vid', "blog_category");
        $tids = $query->execute();

        return [
            '#markup' => create_category_block($tids)
        ];
    }

}